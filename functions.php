<?php
/**
 * Bcm-theme Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package bcm-theme
 */

add_action( 'wp_enqueue_scripts', 'astra_parent_theme_enqueue_styles' );

/**
 * Enqueue scripts and styles.
 */
function astra_parent_theme_enqueue_styles() {
	wp_enqueue_style( 'astra-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'bcm-theme-style',
		get_stylesheet_directory_uri() . '/style.css',
		array( 'astra-style' )
	);

}


 // make checkout fields optional 
 function ace_make_checkout_fields_optional( $fields ) {
	$fields['billing']['billing_phone']['required'] = false;

	return $fields;
}
add_filter( 'woocommerce_checkout_fields', 'ace_make_checkout_fields_optional' );

// lets remove some checkout fields
function js_remove_checkout_fields( $fields ) {

    // Billing fields
	unset( $fields['billing']['billing_address_1'] );
	unset( $fields['billing']['billing_address_2'] );
	unset( $fields['billing']['billing_postcode'] );
	unset( $fields['billing']['billing_city']);
	
	

	return $fields;
}
add_filter( 'woocommerce_checkout_fields', 'js_remove_checkout_fields' );

/* show currently used template */
function meks_which_template_is_loaded() {
	if ( is_super_admin() ) {
		global $template;
		print_r( $template );
	}
}
add_action( 'wp_footer', 'meks_which_template_is_loaded' );


/*
 * Create a column. And maybe remove some of the default ones
 * @param array $columns Array of all user table columns {column ID} => {column Name} 
 */
add_filter( 'manage_users_columns', 'bcm_modify_user_table' );
 
function bcm_modify_user_table( $columns ) {
 
	// unset( $columns['posts'] ); // maybe you would like to remove default columns
	$columns['registration_date'] = 'Registration date'; // add new
	$columns['last_login'] = 'Last Login'; // column ID / column Title
 
	return $columns;
 
}
 
/*
 * Fill our new column with the registration dates of the users
 * @param string $row_output text/HTML output of a table cell
 * @param string $column_id_attr column ID
 * @param int $user user ID (in fact - table row ID)
 */
add_filter( 'manage_users_custom_column', 'bcm_modify_user_table_row', 10, 3 );
 
function bcm_modify_user_table_row( $row_output, $column_id_attr, $user ) {
 
	$date_format = 'j M, Y H:i';
 
	switch ( $column_id_attr ) {
		case 'registration_date' :
			return date( $date_format, strtotime( get_the_author_meta( 'registered', $user ) ) );
			break;
		default:
	}
	
 
	return $row_output;
 
}

add_filter( 'manage_users_custom_column', 'bcm_last_login_column', 10, 3 );

function bcm_last_login_column( $output, $column_id, $user_id ){
 
	if( $column_id == 'last_login' ) {
 
		$last_login = get_user_meta( $user_id, 'last_login', true );
		$date_format = 'j M, Y';
 
		$output = $last_login ? date( $date_format, $last_login ) : '-';
 
	}
 
	return $output;
 
}
/*
 * Make our "Registration date" column sortable
 * @param array $columns Array of all user sortable columns {column ID} => {orderby GET-param} 
 */
add_filter( 'manage_users_sortable_columns', 'bcm_make_registered_column_sortable' );
 
function bcm_make_registered_column_sortable( $columns ) {
	return wp_parse_args( array( 
		'registration_date' => 'registered',
		'last_login' => 'last_login' 
	), $columns );
}


add_action( 'pre_get_users', 'bcm_sort_last_login_column' );

function bcm_sort_last_login_column( $query ) {
 
	if( !is_admin() ) {
		return $query;
	}
 
	$screen = get_current_screen();
 
	if( isset( $screen->id ) && $screen->id !== 'users' ) {
		return $query;
	}
 
	if( isset( $_GET[ 'orderby' ] ) && $_GET[ 'orderby' ] == 'last_login' ) {
 
		$query->query_vars['meta_key'] = 'last_login';
		$query->query_vars['orderby'] = 'meta_value';
 
	}
 
	return $query;
 
}

// show last login in admin
add_action( 'wp_login', 'bcm_collect_login_timestamp', 20, 2 );
 
function bcm_collect_login_timestamp( $user_login, $user ) {
 
	update_user_meta( $user->ID, 'last_login', time() );
 
}


/**
 * Custom register email
 */
// add_filter( 'wp_new_user_notification_email', 'custom_wp_new_user_notification_email', 10, 3 );
// function custom_wp_new_user_notification_email( $wp_new_user_notification_email, $user, $blogname ) {
 
//     $user_login = stripslashes( $user->user_login );
//     $user_email = stripslashes( $user->user_email );
//     $login_url  = wp_login_url();
//     $message  = __( 'Hi there,' ) . "\r\n\r\n";
//     $message .= sprintf( __( "Thanks for registering for %s! Here's how to log in:" ), get_option('blogname') ) . "\r\n\r\n";
//     $message .= wp_login_url() . "\r\n";
//     $message .= sprintf( __('Username: %s'), $user_login ) . "\r\n";
//     $message .= sprintf( __('Email: %s'), $user_email ) . "\r\n";
//     $message .= __( 'Password: The one you entered in the registration form. (For security reason, we save encripted password)' ) . "\r\n\r\n";
//     $message .= sprintf( __('If you have any problems, please contact me at %s.'), get_option('admin_email') ) . "\r\n\r\n";
//     $message .= __( 'bye!' );
 
//     $wp_new_user_notification_email['subject'] = sprintf( '[%s] Your credentials.', $blogname );
//     $wp_new_user_notification_email['headers'] = array('Content-Type: text/html; charset=UTF-8');
//     $wp_new_user_notification_email['message'] = ( strip_tags($message) );
 
//     return $wp_new_user_notification_email;
// }

/**
 * Custom logo on login page
 */
function my_login_logo() { ?>
    <style type="text/css">
		#login { width: 500px!important;}
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png);
		height:195px;
		width:500px;
		background-size: 500px 195px;
		background-repeat: no-repeat;
        	padding-bottom: 121px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );